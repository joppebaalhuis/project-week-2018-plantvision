#!/usr/bin/python
import cv2 
import numpy as np
from picamera import PiCamera
from time import sleep
import RPi.GPIO as GPIO
import time


minGreen = 2
inputPin =  38
outputPinOk = 35
outputPinNok = 33

# take a picture
def takePicture():
	camera = PiCamera()		# initialize the camera
	camera.start_preview()
#	sleep(1)
	camera.capture('picture.jpg')	# write the picture
	camera.stop_preview()
    	camera.close()

# process the picture
def processPicture():
	img=cv2.imread('picture.jpg')
	blur=cv2.GaussianBlur(img,(5,5),0)
	hsv=cv2.cvtColor(blur,cv2.COLOR_BGR2HSV)
	#threshold green
	low_g=np.array([35,100,60],np.uint8)			#low green threshold
	up_g=np.array([85,255,190],np.uint8)			#up green threshold
	mask=cv2.inRange(hsv,low_g,up_g)			#create mask
	mask_upstate=cv2.bitwise_and(blur, blur, mask=mask)	#only allow green values through
	#get the bgr value
	mean=cv2.mean(mask_upstate)				#mean amount of pictures
	print('RGB:'+ str(mean[0]) + ':' + str(mean[1]) + ':' + str(mean[2]))	#print the RGB values
	cv2.imwrite('green.jpg',mask_upstate)
	return (mean,mask_upstate)


# check if the plant is in the picture
def checkForPlant(mean):
	print('checking for plant')
	if(mean[1]  >= minGreen):
		print("\033[0;37;42m found plant  \033[0m   \n")  # print the outcome with a green background to the screen
		GPIO.output(outputPinOk, GPIO.HIGH)		      # give 1 second high signal on the OK pin
		time.sleep(1)
		GPIO.output(outputPinOk,GPIO.LOW)
	else:
		print("\033[0;37;41m no plant found  \033[0m \n") # print the outcome with a red background to the screen
		GPIO.output(outputPinNok,GPIO.HIGH)		  # give 1 second high signal on the NOK pin
		time.sleep (1)
		GPIO.output(outputPinNok, GPIO.LOW)


#show the picture on the screen
def showPicture(mask_upstate):
	print('processing image')
	checkForPlant(mean)
	cv2.imshow('image',mask_upstate)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

#setup
def setup():
	print('setup')
 	GPIO.setwarnings(False)		# disable GPIO warnigns
	GPIO.setmode(GPIO.BOARD)	# set the GPIO pinout to board mode
	GPIO.setup(inputPin, GPIO.IN,pull_up_down=GPIO.PUD_DOWN) #set the input pin al pull down
	GPIO.setup(outputPinOk, GPIO.OUT, initial = 0)		#set the outputPinOk to output with initial value 0
	GPIO.setup(outputPinNok,GPIO.OUT, initial = 0)		#set the outputpinNOk to output with inital value 0


#main
print('program running')
setup()
while(1):
	interrupt = False
	while(interrupt == False):
		if(GPIO.input(inputPin) == GPIO.LOW):
		#	print('waiting for interrupt')
			interrupt = False
			time.sleep(0.30)
		else:
			print('interrupt')
			interrupt = True
	takePicture()
	mean,mask_upstate = processPicture()
	checkForPlant(mean)
#	showPicture(mask_upstate)
#
