# project week 2018 plantVision

Contact: joppe.baalhuis@hotmail.com



#system setup
The system is set up using the following
* raspberry pi 3B+
* raspbian OS kernel version 4.14 (flashed using "Balena Etcher" on 32 Gb SD card)
* python version  2.7.13 (by default installed on the raspian image)
* openCV 2 version 3.4.1 (installed using the instructions below)
* raspberry pi camera
* power supply 5V DC 2A (switching mode)

# the python code should be run a root user
This can be access by typing 
<pre class="prettyprint lang-bash prettyprinted" style="">sudo su</pre>


##Installation of openCV

The installation of openCV is done with according to the following link:
https://www.alatortsev.com/2018/04/27/installing-opencv-on-raspberry-pi-3-b/ (04-02-2019)
Below are the steps from the website:

**Step 1: make sure you have the latest version of OS**

Current OS version is [Raspbian Stretch](https://www.raspberrypi.org/downloads/raspbian/) (April 2018). You can either do a clean install from SD (follow the instructions listed [here](https://www.raspberrypi.org/documentation/installation/installing-images/README.md)), or upgrade your existing version.

To upgrade, open (as sudo) the files _/etc/apt/sources.list_ and _/etc/apt/sources.list.d/raspi.list_ in your favorite editor, and change all occurences of your current distro name (e.g. "jessie") to "stretch". Then, open a terminal and run the update:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo apt-get update
sudo apt-get -y dist-upgrade</pre>

**Step 2: configure SSH and utilities**

Make sure SSH is [enabled](https://www.raspberrypi.org/documentation/remote-access/ssh/). Change the default password, too!

Some of my favorite utilities on Linux are _screen_ (to keep processes running if your terminal session is dropped) and _htop_ (performance monitoring) - these may already be pre-installed:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo apt-get install screen
sudo apt-get install htop</pre>

**Step 3: free up 1GB+ by ditching Wolfram and Libreoffice**

It's unlikely that you will need these two packages on your computer vision box, so:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo apt-get purge wolfram-engine
sudo apt-get purge libreoffice* sudo apt-get clean
sudo apt-get autoremove</pre>

**Step 4: install dependencies**

<pre class="prettyprint lang-bash prettyprinted" style="">sudo apt-get install build-essential cmake pkg-config
sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev
sudo apt-get install libgtk2.0-dev libgtk-3-dev
sudo apt-get install libatlas-base-dev gfortran</pre>

**Step 5: install Python 2.7 & 3**

We need this in order to enable Python [bindings](https://docs.opencv.org/3.2.0/da/d49/tutorial_py_bindings_basics.html) in Open CV:

> In OpenCV, all algorithms are implemented in C++. But these algorithms can be used from different languages like Python, Java etc. This is made possible by the bindings generators. These generators create a bridge between C++ and Python which enables users to call C++ functions from Python. To get a complete picture of what is happening in background, a good knowledge of Python/C API is required. A simple example on extending C++ functions to Python can be found in official Python documentation[1]. So extending all functions in OpenCV to Python by writing their wrapper functions manually is a time-consuming task. So OpenCV does it in a more intelligent way. OpenCV generates these wrapper functions automatically from the C++ headers using some Python scripts which are located in modules/python/src2

<pre class="prettyprint lang-bash prettyprinted" style="">sudo apt-get install python2.7-dev 
sudo apt-get install python3-dev</pre>

**Step 6: get the latest OpenCV source code**

I am using version 3.4.1 of OpenCV. You can check the [Releases](https://opencv.org/releases.html) section of the official site (or Github) to see what the current build is. If your desired version is different, update the commands and paths below accordingly.

Download and unzip OpenCV 3.4.1 and its experimental modules (those are stored in the [opencv_contrib](https://github.com/opencv/opencv_contrib) repository):

<pre class="prettyprint lang-bash prettyprinted" style="">wget -O opencv.zip https://github.com/opencv/opencv/archive/3.4.1.zip wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/3.4.1.zip unzip opencv.zip

unzip opencv_contrib.zip</pre>

**Step 7: install pip and virtualenv**

These are the lowest-level tools for [managing Python packages](https://packaging.python.org/guides/installing-using-pip-and-virtualenv/).

Get pip first:

<pre class="prettyprint lang-bash prettyprinted" style="">wget -O get-pip.py https://bootstrap.pypa.io/get-pip.py 
sudo python get-pip.py
sudo python3 get-pip.py</pre>

Source: [https://pip.pypa.io/en/stable/installing/](https://pip.pypa.io/en/stable/installing/)

Then, install virtual environments:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo pip install virtualenv virtualenvwrapper</pre>

Modify your _~/.profile_ file to include the following lines:

_export WORKONHOME=$HOME/.virtualenvs  
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3  
source /usr/local/bin/virtualenvwrapper.sh_

This is what my file looks like:

<pre class="prettyprint lang-bash prettyprinted" style=""># ~/.profile: executed by the command interpreter for login shells.  # This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login  # exists.  # see /usr/share/doc/bash/examples/startup-files for examples.  # the files are located in the bash-doc package.  # the default umask is set in /etc/profile; for setting the umask  # for ssh logins, install and configure the libpam-umask package.  #umask 022  # if running bash  if  [  -n "$BASH_VERSION"  ];  then  # include .bashrc if it exists  if  [  -f "$HOME/.bashrc"  ];  then  .  "$HOME/.bashrc"  fi  fi  # set PATH so it includes user's private bin if it exists  if  [  -d "$HOME/bin"  ]  ;  then PATH="$HOME/bin:$PATH"  fi  # virtualenv and virtualenvwrapper settings export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh</pre>

... activate the changes:

<pre class="prettyprint lang-bash prettyprinted" style="">source ~/.profile</pre>

**Step 8: create a virtual environment**

<pre class="prettyprint lang-bash prettyprinted" style="">mkvirtualenv cv -p python3</pre>

... or, if you want to use Python 2.7 instead of Python 3:

<pre class="prettyprint lang-bash prettyprinted" style="">mkvirtualenv cv -p python2</pre>

These are the basic commands for working with virtualenvwarapper:

<pre class="prettyprint lang-bash prettyprinted" style="">mkvirtualenv virtualenv_name # create virtualenv workon virtualenv_name # activate/switch to a virtualenv deactivate virtualenv_name # deactivate virtualenv</pre>

In our case, we can activate a virtualenv called "cv":

<pre class="prettyprint lang-bash prettyprinted" style="">pi@raspberrypi:~ $ workon cv (cv) pi@raspberrypi:~ $ </pre>

**Step 9: install Numpy, Scipy**

Now that you are inside your virtual environment (as evidenced by the "(cv)" prefix in your terminal window), let's install some additional packages for data analysis - _[numpy](http://www.numpy.org//)_ and _[scipy](https://www.scipy.org/)_:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo pip install numpy scipy</pre>

**Step 10: finally, install OpenCV**

_Note: this will take a long, long, long time. Took almost 2 hours on my device. Also, your Raspberry Pi will overheat without proper cooling._

Again, I am using version 3.4.1 of OpenCV. If you aren't - update your paths accordingly:

<pre class="prettyprint lang-bash prettyprinted" style="">cd ~/opencv-3.4.1/ mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=RELEASE \ -D CMAKE_INSTALL_PREFIX=/usr/local \ -D INSTALL_PYTHON_EXAMPLES=ON \ -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-3.4.1/modules \ -D BUILD_EXAMPLES=ON ..</pre>

Make sure cmake finishes without errors.

Now, get yourself a glass of beer, and prepare for the final step - compiling. To speed things up, temporarily increase the swap file size in your _/etc/dphys-swapfile_ by changing CONF_SWAPSIZE from 100 to 1024:

<pre class="prettyprint lang-bash prettyprinted" style=""># set size to absolute value, leaving empty (default) then uses computed value  #   you most likely don't want this, unless you have an special disk situation  #CONF_SWAPSIZE=100 CONF_SWAPSIZE=1024</pre>

To avoid rebooting in order for these changes to take effect, simply restart the swap service:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo /etc/init.d/dphys-swapfile restart</pre>

There's a detailed guide on how to compile OpenCV with packages here:

[https://github.com/opencv/opencv_contrib](https://github.com/opencv/opencv_contrib)

What I did (using all 4 CPU cores):

<pre class="prettyprint lang-bash prettyprinted" style="">make -j4</pre>

Once OpenCV compiles successfully, continue the installation:

<pre class="prettyprint lang-bash prettyprinted" style="">sudo make install
sudo ldconfig
sudo apt-get update</pre>

... then reboot the system - and you should be good to go!

One more thing:

Test the installation:

<pre class="prettyprint lang-bash prettyprinted" style="">$ python3 Python  3.5.3  (default,  Jan  19  2017,  14:11:04)  [GCC 6.3.0  20170124] on linux Type  "help",  "copyright",  "credits" or "license"  for more information.  >>> import cv2 >>> cv2.__version__ '3.4.1'  >>>  </pre>

If you are getting an error (ImportError: No module named 'cv2'), the library may be named incorrectly:

<pre class="prettyprint lang-bash prettyprinted" style="">(cv) pi@raspberrypi:$ ls -l /usr/local/lib/python3.5/site-packages/ total 4500  -rw-r--r--  1 root staff 4604912  Apr  27  14:41 cv2.cpython-35m-arm-linux-gnueabihf.so</pre>

Fix it by renaming the library file to "cv2.so":

<pre class="prettyprint lang-bash prettyprinted" style="">cd /usr/local/lib/python3.5/site-packages/ sudo mv cv2.cpython-35m-arm-linux-gnueabihf.so cv2.so</pre>

